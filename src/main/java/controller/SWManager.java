package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import dao.DAOStarWars;
import models.Film;
import models.People;
import models.Starship;
import utilities.APIStarWars;

/**
 * Servlet implementation class SWController
 */
public class SWManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SWManager() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
		//response.getWriter().append("Method now allowed");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String res = "";
		PrintWriter out = null;
		try {
			String action = request.getParameter("action");
			out = response.getWriter();
			if ("importData".equals(action)) {
				importData();
				res = "ok";
				response.setContentType("text/html");
				out.print(res);
			} else if ("getApperenceTables".equals(action)) {
				response.setContentType("application/json");
				JsonArray json = DAOStarWars.getAppearences();
				out.print(json);
			}else if ("getFilms".equals(action)) {
				response.setContentType("application/json");
				JsonArray json = DAOStarWars.getFilms();
				out.print(json);
			}else if ("getPilotStarshipApperence".equals(action)) {
				response.setContentType("application/json");
				String films = request.getParameter("films");
				JsonArray json = DAOStarWars.getPilotStarshipApperence(films);
				out.print(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
			res = e.getMessage();
		}
		out.flush();
	}
	
	private void importData() throws Exception{	
		
		try {
			DAOStarWars.truncateAllTables();
			importFilms();
			importStarships();
			importPeople();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
	}
	
	private void importPeople() throws Exception{
		try {
			importPeople(null);
		} catch (Exception e) {
			throw e;
		}
	}
	
	private void importPeople(String currentUrl) throws Exception{
		try {
			
			JsonObject jsonPeople = new JsonObject();
			if(currentUrl == null) {
				jsonPeople = APIStarWars.getBuilder("people", null);
			}else {
				jsonPeople = APIStarWars.getBuilder(currentUrl);
			}
			
			JsonArray results = jsonPeople.get("results").getAsJsonArray();
			
			String nextUrl = null;
			if(!jsonPeople.get("next").isJsonNull()) {
				nextUrl = jsonPeople.get("next").getAsString();
			}
			
			ArrayList<People> people = new ArrayList<People>();
			
			for (JsonElement person : results) {
				int id = getIdFromUrl(person.getAsJsonObject().get("url").getAsString());
				String name = person.getAsJsonObject().get("name").getAsString();
				String birthYear = person.getAsJsonObject().get("birth_year").getAsString();
				String eyeColor = person.getAsJsonObject().get("eye_color").getAsString();
				String gender = person.getAsJsonObject().get("gender").getAsString();
				String hairColor = person.getAsJsonObject().get("hair_color").getAsString(); 
				String height  = person.getAsJsonObject().get("height").getAsString(); 
				String mass  = person.getAsJsonObject().get("mass").getAsString(); 
				String skinColor = person.getAsJsonObject().get("skin_color").getAsString();
				String homeworld  = person.getAsJsonObject().get("homeworld").getAsString();
				String url = person.getAsJsonObject().get("url").getAsString(); 
				String createdStringDate = person.getAsJsonObject().get("created").getAsString();
				TemporalAccessor ta = DateTimeFormatter.ISO_INSTANT.parse(createdStringDate);
				Instant i = Instant.from(ta);
			    Date created = Date.from(i);		    
			    String editedStringDate = person.getAsJsonObject().get("created").getAsString();
				ta = DateTimeFormatter.ISO_INSTANT.parse(editedStringDate);
				i = Instant.from(ta);
			    Date edited = Date.from(i);
			    
			    ArrayList<Integer> films = new ArrayList<Integer>();
			    JsonArray filmsUrls = person.getAsJsonObject().get("films").getAsJsonArray();
			    for (JsonElement filmUrl : filmsUrls) {
			    	films.add(getIdFromUrl(filmUrl.getAsString()));
				}
			    
			    ArrayList<Integer> starships = new ArrayList<Integer>();
			    JsonArray starshipsUrls = person.getAsJsonObject().get("starships").getAsJsonArray();
			    for (JsonElement starshipUrl : starshipsUrls) {
			    	starships.add(getIdFromUrl(starshipUrl.getAsString()));
				}
			    
			    People p = new People(id, name, birthYear, eyeColor, gender, 
			    		hairColor, height, mass, skinColor, homeworld, films, starships, url, created, edited);
			    people.add(p);
			}
			
			DAOStarWars.insertPeople(people);
			
			if(nextUrl != null) {
				importPeople(nextUrl);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error importing people");
		}
	}
	
	private void importStarships() throws Exception{
		try {
			importStarships(null);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	private void importStarships(String currentUrl) throws Exception{
		try {
			
			JsonObject jsonStarships = new JsonObject();
			if(currentUrl == null) {
				jsonStarships = APIStarWars.getBuilder("starships", null);
			}else {
				jsonStarships = APIStarWars.getBuilder(currentUrl);
			}
			
			JsonArray results = jsonStarships.get("results").getAsJsonArray();
			
			String nextUrl = null;
			if(!jsonStarships.get("next").isJsonNull()) {
				nextUrl = jsonStarships.get("next").getAsString();
			}
			
			ArrayList<Starship> starships = new ArrayList<Starship>();
			
			for (JsonElement starship : results) {
				int id = getIdFromUrl(starship.getAsJsonObject().get("url").getAsString());
				String name = starship.getAsJsonObject().get("name").getAsString();
				String model = starship.getAsJsonObject().get("model").getAsString();
				String starshipClass = starship.getAsJsonObject().get("starship_class").getAsString(); 
				String manufacturer = starship.getAsJsonObject().get("manufacturer").getAsString(); 
				String costInCredits = starship.getAsJsonObject().get("cost_in_credits").getAsString(); 
				String length = starship.getAsJsonObject().get("length").getAsString();
				String crew = starship.getAsJsonObject().get("crew").getAsString();
				String passengers = starship.getAsJsonObject().get("passengers").getAsString(); 
				String maxAtmospheringSpeed = starship.getAsJsonObject().get("max_atmosphering_speed").getAsString(); 
				String hyperdriveRating = starship.getAsJsonObject().get("hyperdrive_rating").getAsString(); 
				String mglt = starship.getAsJsonObject().get("MGLT").getAsString(); 
				String cargoCapacity = starship.getAsJsonObject().get("cargo_capacity").getAsString();
				String consumables = starship.getAsJsonObject().get("consumables").getAsString(); 
				String url = starship.getAsJsonObject().get("url").getAsString(); 
				String createdStringDate = starship.getAsJsonObject().get("created").getAsString();
				TemporalAccessor ta = DateTimeFormatter.ISO_INSTANT.parse(createdStringDate);
				Instant i = Instant.from(ta);
			    Date created = Date.from(i);		    
			    String editedStringDate = starship.getAsJsonObject().get("created").getAsString();
				ta = DateTimeFormatter.ISO_INSTANT.parse(editedStringDate);
				i = Instant.from(ta);
			    Date edited = Date.from(i);
			    ArrayList<Integer> films = new ArrayList<Integer>();
			    
			    JsonArray filmsUrls = starship.getAsJsonObject().get("films").getAsJsonArray();
			    for (JsonElement filmUrl : filmsUrls) {
			    	films.add(getIdFromUrl(filmUrl.getAsString()));
				}
			    
			    Starship s = new Starship(id, name, model, starshipClass, manufacturer, costInCredits, length, crew, passengers, maxAtmospheringSpeed, 
			    		hyperdriveRating, mglt, cargoCapacity, consumables, films, url, created, edited);
			    starships.add(s);
			}	
			
			DAOStarWars.insertStarships(starships);
			
			if(nextUrl != null) {
				importStarships(nextUrl);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error importing starships");
		}
	}
	
	private void importFilms() throws Exception{
		try {
			importFilms(null);
		} catch (Exception e) {
			throw e;
		}
	}
	
	private void importFilms(String currentUrl) throws Exception{
		try {
			
			JsonObject jsonFilms = new JsonObject();
			if(currentUrl == null) {
				jsonFilms = APIStarWars.getBuilder("films", null);
			}else {
				jsonFilms = APIStarWars.getBuilder(currentUrl);
			}
			
			JsonArray results = jsonFilms.get("results").getAsJsonArray();
			
			String nextUrl = null;
			if(!jsonFilms.get("next").isJsonNull()) {
				nextUrl = jsonFilms.get("next").getAsString();
			}
			ArrayList<Film> films = new ArrayList<Film>();
			
			for (JsonElement film : results) {
				int id = getIdFromUrl(film.getAsJsonObject().get("url").getAsString());
				String title = film.getAsJsonObject().get("title").getAsString();
				int episodeId = film.getAsJsonObject().get("episode_id").getAsInt();
				String openingCrawl = film.getAsJsonObject().get("opening_crawl").getAsString();
				String director = film.getAsJsonObject().get("director").getAsString();
				String producer = film.getAsJsonObject().get("producer").getAsString();
				String releaseDate = film.getAsJsonObject().get("release_date").getAsString();
				String url = film.getAsJsonObject().get("url").getAsString();				
				String createdStringDate = film.getAsJsonObject().get("created").getAsString();
				TemporalAccessor ta = DateTimeFormatter.ISO_INSTANT.parse(createdStringDate);
				Instant i = Instant.from(ta);
			    Date created = Date.from(i);		    
			    String editedStringDate = film.getAsJsonObject().get("created").getAsString();
				ta = DateTimeFormatter.ISO_INSTANT.parse(editedStringDate);
				i = Instant.from(ta);
			    Date edited = Date.from(i);
				Film f = new Film(id, episodeId, title, openingCrawl, director, producer, releaseDate, url, created, edited);
				films.add(f);
			}
			
			DAOStarWars.insertFilms(films);
			
			if(nextUrl != null) {
				importFilms(nextUrl);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception("Error importing films");
		}	
		
	}
	
	private int getIdFromUrl(String url) {
		url = url.substring(0, url.length() - 1);
		return Integer.parseInt(url.substring(url.lastIndexOf("/") + 1, url.length()));
	}
}
