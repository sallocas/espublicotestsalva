package controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOOrders;
import models.Order;
import models.OrderAttributesEnum;

/**
 * Servlet implementation class CSVManager
 */
@MultipartConfig(
        fileSizeThreshold   = 1024 * 1024 * 500,  // 500 MB
        maxFileSize         = 1024 * 1024 * 500, 
        maxRequestSize      = 1024 * 1024 * 500, 
        location            = "./"
)
public class CSVManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CSVManager() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Method now allowed");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String result = "ko";
		try {	
			//Reset table
			DAOOrders.truncate();
			
		    ServletContext context = request.getServletContext();
		    String appPath = context.getRealPath("");
		    String rpath = "resources";
		    String path = appPath + rpath;
		    
		    long start = System.currentTimeMillis();  
		    uploadFile(path, request.getPart("fileToUpload").getInputStream());
		    long total = System.currentTimeMillis() - start;
            System.out.println("Upload Time: " + total);
            
            long exportIni = System.currentTimeMillis();
            exportFile(path);
            long finExport = System.currentTimeMillis() - exportIni;
            System.out.println("finExport: " + finExport);
            result = "ok";
            
		} catch (Exception e) {
			result = "ko";
		}
		response.getWriter().append(result);
	}
	
	private static void uploadFile(String path, InputStream input) throws Exception {
		File file = new File(path, "tmp.csv");
		try{
		    Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
		    if(file.exists() && !file.isDirectory()) {
				String filePathToSql = file.getPath().replace("\\","/");
				DAOOrders.loadCSV(filePathToSql);
			}
		}catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public static void exportFile(String path) throws Exception {
		PrintWriter pw = null;
		ArrayList<Order> orders = null;
		try {
            pw = new PrintWriter(new File(path, "ordered.csv"));
            StringBuilder sb = new StringBuilder();
            orders = DAOOrders.exportOrderedById();
            
            OrderAttributesEnum[] listEnums = OrderAttributesEnum.values();
			for (OrderAttributesEnum c : listEnums) {
				sb.append(c.getDisplayName());
				sb.append(",");
			}
			sb.append("\r\n");
			
            for(Order o : orders) {
            	sb.append(o.toString());
            	sb.append("\r\n");
            }
			pw.write(sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}