package utilities;

import java.sql.*;

public class DatabaseUtilities {
	
	private static String URL = ""; //recordar el ?allowLoadLocalInfile=true
	private static String USER = "";
	private static String PASSWORD = "";
	
	public static Connection getConnection() throws Exception{
		Connection con = null;	
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return con;
	}
}
