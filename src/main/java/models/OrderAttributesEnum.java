package models;

public enum OrderAttributesEnum {

	Region("Region"),
	Country("Country"),
	ItemType("Item Type"),
	SalesChannel("Sales Channel"),
	OrderPriority("Order Priority"),
	OrderDate("Order Date"),
	OrderID("Order ID"),
	ShipDate("Ship Date"),
	UnitsSold("Units Sold"),
	UnitPrice("Unit Price"),
	UnitCost("Unit Cost"),
	TotalRevenue("Total Revenue"),
	TotalCost("Total Cost"),
	TotalProfit("Total Profit");
	  
	private String displayName;
	
	private OrderAttributesEnum(String displayName){
		this.displayName = displayName;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
}
