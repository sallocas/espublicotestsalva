package models;

import java.util.ArrayList;
import java.util.Date;

public class People {
	
	private int id;
	private String name;
	private String birthYear;
	private String eyeColor;
	private String gender;
	private String hairColor;
	private String height;
	private String mass;
	private String skinColor;
	private String homeworld;
	private ArrayList<Integer> films;
	private ArrayList<Integer> starships;
	private String url;
	private Date created;
	private Date edited;
	
	public People() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public People(int id, String name, String birthYear, String eyeColor, String gender, String hairColor, String height, String mass,
			String skinColor, String homeworld, ArrayList<Integer> films, ArrayList<Integer> starships, String url,
			Date created, Date edited) {
		super();
		this.id = id;
		this.name = name;
		this.birthYear = birthYear;
		this.eyeColor = eyeColor;
		this.gender = gender;
		this.hairColor = hairColor;
		this.height = height;
		this.mass = mass;
		this.skinColor = skinColor;
		this.homeworld = homeworld;
		this.films = films;
		this.starships = starships;
		this.url = url;
		this.created = created;
		this.edited = edited;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEyeColor() {
		return eyeColor;
	}
	public void setEyeColor(String eyeColor) {
		this.eyeColor = eyeColor;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	public String getHairColor() {
		return hairColor;
	}
	public void setHairColor(String hairColor) {
		this.hairColor = hairColor;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getMass() {
		return mass;
	}
	public void setMass(String mass) {
		this.mass = mass;
	}
	public String getSkinColor() {
		return skinColor;
	}
	public void setSkinColor(String skinColor) {
		this.skinColor = skinColor;
	}
	public String getHomeworld() {
		return homeworld;
	}
	public void setHomeworld(String homeworld) {
		this.homeworld = homeworld;
	}
	public ArrayList<Integer> getFilms() {
		return films;
	}
	public void setFilms(ArrayList<Integer> films) {
		this.films = films;
	}
	public ArrayList<Integer> getStarships() {
		return starships;
	}
	public void setStarships(ArrayList<Integer> starships) {
		this.starships = starships;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getEdited() {
		return edited;
	}
	public void setEdited(Date edited) {
		this.edited = edited;
	}

}
