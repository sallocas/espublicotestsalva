package models;

public class Order {

	private String Region;
	private String Country;
	private String ItemType;
	private String SalesChannel;
	private String OrderPriority;
	private String OrderDate;
	private int OrderID;
	private String ShipDate;
	private int UnitsSold;
	private double UnitPrice;
	private double UnitCost;
	private double TotalRevenue;
	private double TotalCost;
	private double TotalProfit;
	
	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Order(String region, String country, String itemType, String salesChannel, String orderPriority,
			String orderDate, int orderID, String shipDate, int unitsSold, double unitPrice, double unitCost,
			double totalRevenue, double totalCost, double totalProfit) {
		super();
		Region = region;
		Country = country;
		ItemType = itemType;
		SalesChannel = salesChannel;
		OrderPriority = orderPriority;
		OrderDate = orderDate;
		OrderID = orderID;
		ShipDate = shipDate;
		UnitsSold = unitsSold;
		UnitPrice = unitPrice;
		UnitCost = unitCost;
		TotalRevenue = totalRevenue;
		TotalCost = totalCost;
		TotalProfit = totalProfit;
	}

	public int getOrderID() {
		return OrderID;
	}
	public void setOrderID(int orderID) {
		OrderID = orderID;
	}
	public String getShipDate() {
		return ShipDate;
	}
	public void setShipDate(String shipDate) {
		ShipDate = shipDate;
	}
	public int getUnitsSold() {
		return UnitsSold;
	}
	public void setUnitsSold(int unitsSold) {
		UnitsSold = unitsSold;
	}
	public Double getUnitPrice() {
		return UnitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		UnitPrice = unitPrice;
	}

	public String getRegion() {
		return Region;
	}

	public void setRegion(String region) {
		Region = region;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getItemType() {
		return ItemType;
	}

	public void setItemType(String itemType) {
		ItemType = itemType;
	}

	public String getSalesChannel() {
		return SalesChannel;
	}

	public void setSalesChannel(String salesChannel) {
		SalesChannel = salesChannel;
	}

	public String getOrderPriority() {
		return OrderPriority;
	}

	public void setOrderPriority(String orderPriority) {
		OrderPriority = orderPriority;
	}

	public String getOrderDate() {
		return OrderDate;
	}

	public void setOrderDate(String orderDate) {
		OrderDate = orderDate;
	}

	public double getUnitCost() {
		return UnitCost;
	}

	public void setUnitCost(double unitCost) {
		UnitCost = unitCost;
	}

	public double getTotalRevenue() {
		return TotalRevenue;
	}

	public void setTotalRevenue(double totalRevenue) {
		TotalRevenue = totalRevenue;
	}

	public double getTotalCost() {
		return TotalCost;
	}

	public void setTotalCost(double totalCost) {
		TotalCost = totalCost;
	}

	public double getTotalProfit() {
		return TotalProfit;
	}

	public void setTotalProfit(double totalProfit) {
		TotalProfit = totalProfit;
	}

	public void setUnitPrice(double unitPrice) {
		UnitPrice = unitPrice;
	}
	
	public String toString() {
		return 	Region + "," + 
				Country + "," +
				ItemType + "," +
				SalesChannel + "," +
				OrderPriority + "," +
				OrderDate + "," +
				OrderID + "," +
				ShipDate + "," +
				UnitsSold + "," +
				UnitPrice + "," +
				UnitCost + "," +
				TotalRevenue + "," +
				TotalCost + "," +
				TotalProfit;
	}
}
