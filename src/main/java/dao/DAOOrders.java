package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import models.Order;
import utilities.DatabaseUtilities;

public class DAOOrders {

	public static Map<String, Integer> getNumOrdersByColumn(String column) {

		Connection con = null;
		PreparedStatement statement = null;
		Map<String, Integer> valueCount = null;
		
		try {
			con = DatabaseUtilities.getConnection();
			String sql = "SELECT `" + column + "`, count(*) FROM import_csv GROUP BY `" + column + "`";
			statement = con.prepareStatement(sql);
			
			ResultSet rs = statement.executeQuery();
			
			valueCount = new HashMap<String, Integer>();
			while (rs.next()) {
				valueCount.put(rs.getString(1), rs.getInt(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				statement.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return valueCount;
	}
	
	public static ArrayList<String> getAllColumns() {
		Connection con = null;
		PreparedStatement statement = null;
		ArrayList<String> columns = null;
		
		try {
			con = DatabaseUtilities.getConnection();
			String sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA= 'test_espublico'AND TABLE_NAME='import_csv';";
			statement = con.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			
			columns = new ArrayList<String>();
			
			while (rs.next()) {
				columns.add(rs.getString(1));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				statement.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return columns;
	}
	
	public static void truncate() throws Exception {
		Connection con = null;
		PreparedStatement statement = null;		
		try {
			con = DatabaseUtilities.getConnection();
			String sql = "TRUNCATE TABLE import_csv";
			statement = con.prepareStatement(sql);
			statement.execute();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally {
			try {
				statement.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void loadCSV(String path) throws Exception {
		Connection con = null;
		Statement statement = null;
		
		try {
			con = DatabaseUtilities.getConnection();
			statement = con.createStatement();
			statement.executeUpdate( "LOAD DATA LOCAL INFILE '" + path + "' INTO TABLE  import_csv FIELDS TERMINATED BY ',' LINES TERMINATED BY '\\n'");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally {
			try {
				statement.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static ArrayList<Order> exportOrderedById() throws Exception {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		ArrayList<Order> orders = new ArrayList<Order>();
		
		try {
			String query="SELECT * FROM import_csv ORDER BY `OrderID`;";
			con = DatabaseUtilities.getConnection();
			ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            
            while (rs.next()) {
            	Order o = new Order();           	
            	o.setRegion(rs.getString("Region"));
            	o.setCountry(rs.getString("Country"));
				o.setItemType(rs.getString("ItemType"));
				o.setSalesChannel(rs.getString("SalesChannel"));
				o.setOrderPriority(rs.getString("OrderPriority"));
				o.setOrderDate(rs.getString("OrderDate"));
				o.setOrderID(rs.getInt("OrderID"));
				o.setShipDate(rs.getString("ShipDate"));
				o.setUnitsSold(rs.getInt("UnitsSold"));
				o.setUnitPrice(rs.getDouble("UnitPrice"));
				o.setUnitCost(rs.getDouble("UnitCost"));
				o.setTotalRevenue(rs.getDouble("TotalRevenue"));
				o.setTotalCost(rs.getDouble("TotalCost"));
				o.setTotalProfit(rs.getDouble("TotalProfit"));
				orders.add(o);
			}
   
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally {
			try {
				ps.close();
				rs.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return orders;
	}
}
