package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import models.Film;
import models.People;
import models.Starship;
import utilities.DatabaseUtilities;

public class DAOStarWars {
	
	@SuppressWarnings("resource")
	public static void truncateAllTables() throws Exception {
		Connection con = null;
		PreparedStatement statement = null;		
		try {
			con = DatabaseUtilities.getConnection();
			String sql = "SET FOREIGN_KEY_CHECKS = 0;";
			statement = con.prepareStatement(sql);
			statement.execute();
			
			sql = "TRUNCATE TABLE people";
			statement = con.prepareStatement(sql);
			statement.execute();
			
			sql = "TRUNCATE TABLE starships";
			statement = con.prepareStatement(sql);
			statement.execute();
			
			sql = "TRUNCATE TABLE films";
			statement = con.prepareStatement(sql);
			statement.execute();
			
			con = DatabaseUtilities.getConnection();
			sql = "TRUNCATE TABLE films_has_starships";
			statement = con.prepareStatement(sql);
			statement.execute();
			
			sql = "TRUNCATE TABLE people_has_films";
			statement = con.prepareStatement(sql);
			statement.execute();
			
			sql = "TRUNCATE TABLE people_has_starships";
			statement = con.prepareStatement(sql);
			statement.execute();
			
			sql = "SET FOREIGN_KEY_CHECKS = 1;";
			statement = con.prepareStatement(sql);
			statement.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally {
			try {
				statement.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void insertFilms(ArrayList<Film> films) throws Exception {
		Connection con = null;
		PreparedStatement statement = null;		
		try {
			con = DatabaseUtilities.getConnection();
			StringBuffer sql = new StringBuffer("INSERT INTO test_espublico.films (id, title, episode_id, opening_crawl, director, producer, release_date, url, created, edited) VALUES");
			
			for(int i = 0; i < films.size(); i++) {
				//TODO Change to preparedStatement
				if(i != 0) {
					sql.append(",");
				}
				sql.append("("  + films.get(i).getId()+ ", "
								+ "\"" + films.get(i).getTitle() + "\", "
								+ films.get(i).getEpisodeId() +", "
								+ "\"" + films.get(i).getOpeningCrawl() +"\", "
								+ "\"" + films.get(i).getDirector() +"\", "
								+ "\"" + films.get(i).getProducer() + "\", "
								+ films.get(i).getReleaseDate() + ", "
								+ "\"" + films.get(i).getUrl() + "\", " 
								+ "\"" + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(films.get(i).getCreated()) + "\", "
								+ "\"" + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(films.get(i).getEdited()) +"\")");
			}
			
			statement = con.prepareStatement(sql.toString());		
			statement.execute();			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally {
			try {
				statement.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("resource")
	public static void insertStarships(ArrayList<Starship> starships) throws Exception {
		Connection con = null;
		PreparedStatement statement = null;		
		try {
			con = DatabaseUtilities.getConnection();
			StringBuffer sql = new StringBuffer("INSERT INTO `starships` (`id`, `name`, `model`, `starship_class`, `manufacturer`, `cost_in_credits`, `length`, "
					+ "`crew`, `passengers`, `max_atmosphering_speed`, `hyperdrive_rating`, `MGLT`, `cargo_capacity`, "
					+ "`consumables`, `url`, `created`, `edited`)\r\n"
					+ "VALUES");
			
			for(int i = 0; i < starships.size(); i++) {
				//TODO Change to preparedStatement
				if(i != 0) {
					sql.append(",");
				}
				sql.append("("  + starships.get(i).getId()+ ", "
								+ "\"" + starships.get(i).getName() + "\", "
								+ "\"" + starships.get(i).getModel() + "\", "
								+ "\"" + starships.get(i).getStarshipClass() + "\", "
								+ "\"" + starships.get(i).getManufacturer() + "\", "
								+ "\"" + starships.get(i).getCostInCredits() + "\", "
								+ "\"" + starships.get(i).getLength() + "\", "
								+ "\"" + starships.get(i).getCrew() + "\", "
								+ "\"" + starships.get(i).getPassengers() + "\", "
								+ "\"" + starships.get(i).getMaxAtmospheringSpeed() + "\", "
								+ "\"" + starships.get(i).getHyperdriveRating() + "\", "
								+ "\"" + starships.get(i).getMglt() + "\", "
								+ "\"" + starships.get(i).getCargoCapacity() + "\", "
								+ "\"" + starships.get(i).getConsumables() + "\", "
								+ "\"" + starships.get(i).getUrl() + "\", "
								+ "\"" + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(starships.get(i).getCreated()) + "\", "
								+ "\"" + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(starships.get(i).getEdited()) +"\")");
			}
			statement = con.prepareStatement(sql.toString());		
			statement.execute();			
			
			//Once we have all the starships inserted, we create the films-starship relationship
			sql = new StringBuffer("INSERT INTO films_has_starships (starships_id, films_id) VALUES ");
			boolean isEmpty = true;
			for(int i = 0; i < starships.size(); i++) {
				for(int j = 0; j < starships.get(i).getFilms().size(); j++) {
					isEmpty = false;
					sql.append("("  + starships.get(i).getId()+ ", "
									+ starships.get(i).getFilms().get(j)+ "),");
				}
			}
			if(!isEmpty) {
				statement = con.prepareStatement(sql.toString().substring(0, sql.toString().length()-1));		
				statement.execute();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally {
			try {
				statement.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("resource")
	public static void insertPeople(ArrayList<People> people) throws Exception {
		Connection con = null;
		PreparedStatement statement = null;		
		try {
			con = DatabaseUtilities.getConnection();
			StringBuffer sql = new StringBuffer("INSERT INTO `people` (`id`, `name`, `birth_year`, `eye_color`, `gender`, `hair_color`, `height`, `mass`, `skin_color`, `homeworld`, `url`, `created`, `edited`)\r\n"
					+ "VALUES ");
			
			for(int i = 0; i < people.size(); i++) {
				//TODO Change to preparedStatement
				if(i != 0) {
					sql.append(",");
				}
				sql.append("("  + people.get(i).getId()+ ", "
								+ "\"" + people.get(i).getName() + "\", "
								+ "\"" + people.get(i).getBirthYear() + "\", "
								+ "\"" + people.get(i).getEyeColor() + "\", "
								+ "\"" + people.get(i).getGender() + "\", "
								+ "\"" + people.get(i).getHairColor() + "\", "
								+ "\"" + people.get(i).getHeight() + "\", "
								+ "\"" + people.get(i).getMass() + "\", "
								+ "\"" + people.get(i).getSkinColor() + "\", "
								+ "\"" + people.get(i).getHomeworld() + "\", "
								+ "\"" + people.get(i).getUrl() + "\", "
								+ "\"" + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(people.get(i).getCreated()) + "\", "
								+ "\"" + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(people.get(i).getEdited()) +"\")");
			}
			statement = con.prepareStatement(sql.toString());		
			statement.execute();			
			
			//Once we have all the people inserted, we create the films-people relationship
			sql = new StringBuffer("INSERT INTO people_has_films (people_id, films_id) VALUES ");
			boolean isEmpty = true;
			for(int i = 0; i < people.size(); i++) {
				for(int j = 0; j < people.get(i).getFilms().size(); j++) {
					isEmpty = false;
					sql.append("("  + people.get(i).getId()+ ", "
									+ people.get(i).getFilms().get(j)+ "),");
				}
			}
			if(!isEmpty) {
				statement = con.prepareStatement(sql.toString().substring(0, sql.toString().length()-1));
				statement.execute();
			}
			
			
			sql = new StringBuffer("INSERT INTO people_has_starships (people_id, starships_id) VALUES ");
			isEmpty = true;
			for(int i = 0; i < people.size(); i++) {
				for(int j = 0; j < people.get(i).getStarships().size(); j++) {
					isEmpty = false;
					sql.append("("  + people.get(i).getId()+ ", "
									+ people.get(i).getStarships().get(j)+ "),");
				}
			}
			if(!isEmpty) {
				statement = con.prepareStatement(sql.toString().substring(0, sql.toString().length()-1));
				statement.execute();
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally {
			try {
				statement.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	
	public static JsonArray getAppearences() throws Exception {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DatabaseUtilities.getConnection();
			StringBuffer sql = new StringBuffer("SELECT p.name as Person, COUNT(*) as Appearances, GROUP_CONCAT(f.title) as Films\r\n"
					+ "FROM people_has_films pf, people p, films f\r\n"
					+ "WHERE \r\n"
					+ "pf.people_id = p.id and\r\n"
					+ "pf.films_id = f.id\r\n"
					+ "GROUP BY pf.people_id\r\n"
					+ "ORDER BY Appearances DESC");
			statement = con.prepareStatement(sql.toString());		
			ResultSet rs = statement.executeQuery();
			JsonArray jsonResult = new JsonArray();
			while (rs.next()) {
				
				JsonObject jsonRow = new JsonObject();
				
				jsonRow.addProperty("person", rs.getString(1));
				jsonRow.addProperty("appearances", rs.getInt(2));
				jsonRow.addProperty("films", rs.getString(3));

				jsonResult.add(jsonRow);
			}
			return jsonResult;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally {
			try {
				statement.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static JsonArray getFilms() throws Exception {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DatabaseUtilities.getConnection();
			StringBuffer sql = new StringBuffer("SELECT id, title FROM films");
			statement = con.prepareStatement(sql.toString());		
			ResultSet rs = statement.executeQuery();
			JsonArray jsonResult = new JsonArray();
			while (rs.next()) {
				
				JsonObject jsonRow = new JsonObject();
				
				jsonRow.addProperty("id", rs.getInt(1));
				jsonRow.addProperty("title", rs.getString(2));

				jsonResult.add(jsonRow);
			}
			return jsonResult;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally {
			try {
				statement.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static JsonArray getPilotStarshipApperence(String films) throws Exception {
		Connection con = null;
		PreparedStatement statement = null;
		try {
			con = DatabaseUtilities.getConnection();
			StringBuffer sql = new StringBuffer("SELECT (SELECT name FROM test_espublico.people WHERE id = people_id) as person,  (SELECT name FROM test_espublico.starships WHERE id = starships_id) as starship \r\n"
					+ "FROM test_espublico.people_has_starships \r\n"
					+ "WHERE starships_id in (\r\n"
					+ "SELECT starships_id FROM (\r\n"
					+ "	SELECT starships_id, COUNT(*) as c\r\n"
					+ "	FROM test_espublico.films_has_starships\r\n"
					+ "	WHERE films_id in (" + films + ")\r\n"
					+ "	GROUP BY starships_id\r\n"
					+ "	order by c DESC\r\n"
					+ "	) AS d WHERE d.c = (SELECT MAX(c) \r\n"
					+ "						FROM (\r\n"
					+ "							SELECT starships_id, COUNT(*) as c\r\n"
					+ "							FROM test_espublico.films_has_starships\r\n"
					+ "							WHERE films_id in (" + films + ")\r\n"
					+ "							GROUP BY starships_id\r\n"
					+ "							order by c DESC\r\n"
					+ "							)as b)\r\n"
					+ ")");
			statement = con.prepareStatement(sql.toString());		
			ResultSet rs = statement.executeQuery();
			JsonArray jsonResult = new JsonArray();
			while (rs.next()) {
				
				JsonObject jsonRow = new JsonObject();
				
				jsonRow.addProperty("person", rs.getString(1));
				jsonRow.addProperty("starship", rs.getString(2));

				jsonResult.add(jsonRow);
			}
			return jsonResult;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally {
			try {
				statement.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
