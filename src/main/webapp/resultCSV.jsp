<%@page import="models.OrderAttributesEnum"%>
<%@page import="javax.swing.text.StyledEditorKit.ForegroundAction"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dao.DAOOrders"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<title>esPublico Salva</title>
</head>
<body>

	<div class="container">

		<div class="row">

			<h1>Your result</h1>

		</div>

		<div class="row">
			<div class="col-md-3">
				<button class="btn btn-success" onclick="window.open('/resources/ordered.csv')">Download Ordered CSV</button>		
			</div>
		</div>

		<div class="row mt-3">
			<select class="form-select" name="orders" id="orders" onchange="getNumResults()">
				<option selected>Choose one option</option>
				<%
					OrderAttributesEnum[] listEnums = OrderAttributesEnum.values();
					for (OrderAttributesEnum c : listEnums) {
				%>
					<option value="<%=c%>"><%=c.getDisplayName()%></option>
				<%
					}
				%>
			</select>
		</div>

		<div class="row" id="tableResults">
			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th scope="col">Value</th>
						<th scope="col">Count</th>
					</tr>
				</thead>
				<tbody id="tableBody">
					<!-- Content generated -->
				</tbody>
			</table>
		</div>
	</div>

	<script type="text/javascript">

		function getNumResults() {
			var column = document.getElementById("orders").value;

			$.ajax({
				url : '/OrdersManager',
				data : {
					"column" : column
				},
				method : 'POST',
				success : function(res) {
					
					$("#tableBody").empty();
					
					Object.entries(res).forEach((entry) => {
							let value = entry[0];
							let count = entry[1];						
							let row = `<tr><th scope="row">` + value + `</th><td>` + count + `</td></tr>`;
							$("#tableBody").append(row);
					});
				}
			});
		}
	</script>

	<!-- Option 1: Bootstrap Bundle with Popper -->
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
		crossorigin="anonymous"></script>

</body>
</html>