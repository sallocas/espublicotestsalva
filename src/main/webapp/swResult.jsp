<%@page import="models.OrderAttributesEnum"%>
<%@page import="javax.swing.text.StyledEditorKit.ForegroundAction"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dao.DAOOrders"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<title>esPublico Salva</title>
</head>
<body>

	<div class="container">
	
	
	<div class="accordion accordion-flush" id="accordionFlushExample">
	  <div class="accordion-item">
	    <h2 class="accordion-header" id="flush-headingOne">
	      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
	        Exercise 1.
	      </button>
	    </h2>
	    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
	      <div class="accordion-body">
				
						<div class="row mt-3" id="tableResults">
							<table class="table">
								<thead class="thead-dark">
									<tr>
										<th scope="col">Person</th>
										<th scope="col">Appearances</th>
										<th scope="col">Films</th>
									</tr>
								</thead>
								<tbody id="tableBody">
									<!-- Content generated -->
								</tbody>
							</table>
						</div>
				
		  </div>
	    </div>
	  </div>
	  <div class="accordion-item">
	    <h2 class="accordion-header" id="flush-headingTwo">
	      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
	        Exercise 2.
	      </button>
	    </h2>
	    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
	      <div class="accordion-body">
	      
			      <div class="row" style="text-align: center">		
					<div class="alert alert-primary col-6" role="alert">
					 In case of draw, we show all the occurrences.
					</div>
				</div>

				<div class="row">
					<div class="col-6">
						<select id="filmsSelect" class="form-select" multiple aria-label="multiple select example">
						
						</select>
					</div>
					
					<div class="col-6">
						<button type="submit" class="btn btn-primary" id="loadPilotStarshipTableButton" onclick="loadPilotStarshipTable()">Load Pilot Starship Table</button>
					</div>
					
					<div class="row mt-3" id="tablePilotStarShip">
						<table class="table">
							<thead class="thead-dark">
								<tr>
									<th scope="col">Person</th>
									<th scope="col">Starship</th>
								</tr>
							</thead>
							<tbody id="tablePilotStarShipBody">
								<!-- Content generated -->
							</tbody>
						</table>
					</div>
				</div>
		  </div>
	    </div>
	  </div>
	</div>
	
	

		






	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			getAppearancesTable();
			getFilms();
		});
		
		function loadPilotStarshipTable(){
			$.ajax({
				url : '/SWManager',
				data : {
					"action" : "getPilotStarshipApperence",
					"films" : $("#filmsSelect").val().toString()
				},
				method : 'POST',
				success : function(res) {
					
					$("#tablePilotStarShipBody").empty();
					
					Object.entries(res).forEach((entry) => {
							let person = entry[1].person;
							let starship = entry[1].starship;
							let row = `<tr><td>` + person + `</td><td>` + starship + `</td></tr>`;
							$("#tablePilotStarShipBody").append(row);
					});
				}
			});
		}
		
		function getFilms(){
			$.ajax({
				url : '/SWManager',
				data : {
					"action" : "getFilms"
				},
				method : 'POST',
				success : function(res) {
					
					$("#tableBody").empty();
					
					Object.entries(res).forEach((entry) => {
							let id = entry[1].id;
							let title = entry[1].title;
							let row = `<option value="` + id + `">` + title + `</option>`;
							$("#filmsSelect").append(row);
					});
				}
			});
		}

		function getAppearancesTable() {
			$.ajax({
				url : '/SWManager',
				data : {
					"action" : "getApperenceTables"
				},
				method : 'POST',
				success : function(res) {
					
					$("#tableBody").empty();
					
					Object.entries(res).forEach((entry) => {
							let person = entry[1].person;
							let appearances = entry[1].appearances;	
							let films = entry[1].films;
							let row = `<tr><td>` + person + `</td><td>` + appearances + `</td><td>` + films + `</td></tr>`;
							$("#tableBody").append(row);
					});
				}
			});
		}
	</script>

	<!-- Option 1: Bootstrap Bundle with Popper -->
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
		crossorigin="anonymous"></script>

</body>
</html>