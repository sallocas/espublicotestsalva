CREATE TABLE `import_csv` (
  `Region` varchar(200) NOT NULL,
  `Country` varchar(200) NOT NULL,
  `ItemType` varchar(200) NOT NULL,
  `SalesChannel` varchar(200) NOT NULL,
  `OrderPriority` varchar(200) NOT NULL,
  `OrderDate` varchar(200) NOT NULL,
  `OrderID` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `ShipDate` varchar(200) NOT NULL,
  `UnitsSold` int NOT NULL,
  `UnitPrice` double NOT NULL,
  `UnitCost` double NOT NULL,
  `TotalRevenue` double NOT NULL,
  `TotalCost` double NOT NULL,
  `TotalProfit` double NOT NULL
);

SET GLOBAL local_infile = true;


-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `films`;
CREATE TABLE `films` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `episode_id` int(11) DEFAULT NULL,
  `opening_crawl` text,
  `director` varchar(45) DEFAULT NULL,
  `producer` varchar(250) DEFAULT NULL,
  `release_date` varchar(45) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `edited` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `films_has_starships`;
CREATE TABLE `films_has_starships` (
  `films_id` int(11) NOT NULL,
  `starships_id` int(11) NOT NULL,
  PRIMARY KEY (`films_id`,`starships_id`),
  KEY `fk_films_has_starships_starships1_idx` (`starships_id`),
  KEY `fk_films_has_starships_films_idx` (`films_id`),
  CONSTRAINT `fk_films_has_starships_films` FOREIGN KEY (`films_id`) REFERENCES `films` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_films_has_starships_starships1` FOREIGN KEY (`starships_id`) REFERENCES `starships` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `import_csv`;
CREATE TABLE `import_csv` (
  `Region` varchar(200) NOT NULL,
  `Country` varchar(200) NOT NULL,
  `ItemType` varchar(200) NOT NULL,
  `SalesChannel` varchar(200) NOT NULL,
  `OrderPriority` varchar(200) NOT NULL,
  `OrderDate` varchar(200) NOT NULL,
  `OrderID` int(11) NOT NULL AUTO_INCREMENT,
  `ShipDate` varchar(200) NOT NULL,
  `UnitsSold` int(11) NOT NULL,
  `UnitPrice` double NOT NULL,
  `UnitCost` double NOT NULL,
  `TotalRevenue` double NOT NULL,
  `TotalCost` double NOT NULL,
  `TotalProfit` double NOT NULL,
  PRIMARY KEY (`OrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `people`;
CREATE TABLE `people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `birth_year` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `eye_color` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `gender` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `hair_color` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `height` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `mass` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `skin_color` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `homeworld` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `url` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `created` date DEFAULT NULL,
  `edited` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


DROP TABLE IF EXISTS `people_has_films`;
CREATE TABLE `people_has_films` (
  `people_id` int(11) NOT NULL,
  `films_id` int(11) NOT NULL,
  PRIMARY KEY (`people_id`,`films_id`),
  KEY `fk_people_has_films_films1_idx` (`films_id`),
  KEY `fk_people_has_films_people1_idx` (`people_id`),
  CONSTRAINT `fk_people_has_films_films1` FOREIGN KEY (`films_id`) REFERENCES `films` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_people_has_films_people1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `people_has_starships`;
CREATE TABLE `people_has_starships` (
  `people_id` int(11) NOT NULL,
  `starships_id` int(11) NOT NULL,
  PRIMARY KEY (`people_id`,`starships_id`),
  KEY `fk_people_has_starships_starships1_idx` (`starships_id`),
  KEY `fk_people_has_starships_people1_idx` (`people_id`),
  CONSTRAINT `fk_people_has_starships_people1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_people_has_starships_starships1` FOREIGN KEY (`starships_id`) REFERENCES `starships` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `starships`;
CREATE TABLE `starships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `model` varchar(45) NOT NULL,
  `starship_class` varchar(45) DEFAULT NULL,
  `manufacturer` varchar(250) DEFAULT NULL,
  `cost_in_credits` varchar(45) DEFAULT NULL,
  `length` varchar(45) DEFAULT NULL,
  `crew` varchar(45) DEFAULT NULL,
  `passengers` varchar(45) DEFAULT NULL,
  `max_atmosphering_speed` varchar(45) DEFAULT NULL,
  `hyperdrive_rating` varchar(45) DEFAULT NULL,
  `MGLT` varchar(45) DEFAULT NULL,
  `cargo_capacity` varchar(45) DEFAULT NULL,
  `consumables` varchar(45) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `edited` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `test_table`;
CREATE TABLE `test_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `short_desc` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2022-04-21 13:00:05
